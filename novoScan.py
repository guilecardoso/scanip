import socket
import subprocess
import sys, argparse
import os
from netaddr import *
from datetime import datetime

# Funcao para validar o ip
def validaIp(valor):
	if(int(valor[2])<0 or int(valor[2])>254):
		print("Ip invalido!")
		sys.exit()
	if(int(valor[3])<0 or int(valor[3])>254):
		print("Ip invalido!")
		sys.exit()

# Funcao para escrever o cabecalho
def cabecalho(ip):
	print ("-" * 65)
	print ("Aguarde, scaneando host remoto ", ip)
	print ("-" * 65)
	if(args.lfile is not None):
		arq.write ("-" * 65)
		arq.write ("\n")
		arq.write ("Aguarde, scaneando host remoto " + ip)
		arq.write ("\n")
		arq.write ("-" * 65)
		arq.write ("\n")

# Limpa a tela
subprocess.call('cls', shell=True)

parser = argparse.ArgumentParser(
	description='''\
Scaner de portas abertas por ip.
--------------------------------
%(prog)s pode ser usado sem agumentos, ira abrir o programa.
O argumento -l grava o resultado em um arquivo com a extensao txt.
Os argumentos -i e -p devem obrigatoriamente ser usados juntos.
''',
	formatter_class=argparse.RawDescriptionHelpFormatter,
	epilog='''\
Exemplos:
scanIps.exe -l log
-> Resultara em um arquivo "log.txt" gravado no mesmo local do programa
scanIps.exe -i 192.168.1.100 -p 5800
-> Ira escanear a porta 5800 do ip 192.168.1.100
scanIps.exe -i 192.168.2.20-35 -p 21,80
-> Ira escanear as portas 21 e 80 dos ips 192.168.2.20 ate 192.168.2.35
scanIps.exe -i 192.168.5.200/24 -p 5800-5810
-> Ira escanear as portas de 5800 ate 5810 dos ips entre 192.168.5.0 ate 192.168.5.254
''',
	prog='scanIps',
	usage='%(prog)s [-h] [-l arquivo] [-i IP -p PORTA]')
parser.add_argument('-l','--lfile', help='Nome do arquivo de Log')
parser.add_argument('-i','--irange',help='Range de ip')
parser.add_argument('-p','--prange',help='Range de portas')
args = parser.parse_args()

if(args.irange is not None and args.prange is None):
	print("precisa do argumento de portas")
	sys.exit()
if(args.irange is None and args.prange is not None):
	print("precisa do argumento de ips")
	sys.exit()
if(args.lfile is not None):
	arq = open(args.lfile+".txt","w")

# PERGUNTA quais os ips a serem varridos
if(args.irange is not None):
	remoteServer    = args.irange
else:
	# Gerando os exemplos de input dos ips
	print ("-" * 65)
	print ("Voce pode inserir os ips da seguinte maneira")
	print ("")
	print ("Ex.: 192.168.1.1-100 ----> Para varrer os ips de 1 ate 100")
	print ("Ex.: 192.168.1.1/16  ----> Para varrer os ips de 1.1 ate 254.254")
	print ("")
	print ("E as portas assim")
	print ("")
	print ("Ex.: 5800-5810         ----> Para varrer as portas de 5800 ate 5810")
	print ("Ex.: 80,5800,447       ----> Para varrer as portas 80, a 5800 e a 447")
	print ("Ex.: 80,5800-5810,447  ----> Para varrer as portas 80, de 5800 ate 5810 e a 447")
	print ("")
	print ("-" * 65)
	print ("")
	remoteServer    = input("Digite o host remoto para scanear: ")

# Inicio das variaveis de verificacao
numHosts=None
allHosts=None
allPorts=None

# Validacao se host é nulo
if remoteServer == "":
	print("Nao foi digitado nenhum ip!")
	if(args.lfile is not None):
		arq.write('Nao foi digitado nenhum ip!')
		arq.write ("\n")
	sys.exit()

# INICIO do bloco que identifica quantos hosts foram digitados usando o "-" como range
if remoteServer.find("-") > 0:
	startHost,endHost = remoteServer.split("-")
	ips = startHost.split(".")
	
	# Validacao do ip
	validaIp(ips)
	
	# Verifica se o ultimo digito do ultimo ip eh superior ao ultimo digito do primeiro ip
	if int(endHost)<=int(ips[3]):
		print("Range final deve ser superior ao range inicial!")
		if(args.lfile is not None):
			arq.write("Range final deve ser superior ao range inicial!")
			arq.write ("\n")
		sys.exit()
	else:
		numHosts = int(endHost) - int(ips[3])
	
	endIp = [ips[0],ips[1],ips[2],endHost]
	ip2 = ".".join(endIp)
	lastHost = ip2
	allHosts = list(iter_iprange(startHost,lastHost,step=1))
	
# INICIO do bloco que identifica quantos hosts foram digitados usando a "/" como range
elif remoteServer.find("/") > 0:
	startHost,rangeHost = remoteServer.split("/")
	ips = startHost.split(".")
	# Validacao do ip
	validaIp(ips)
	
	if rangeHost == "16":
		allHosts = [ip for ip in IPNetwork(remoteServer)]
		print("Funcao para o barra 16")
	if rangeHost == "24":
		allHosts = [ip for ip in IPNetwork(remoteServer)]
		print("Funcao para o barra 24")

# INICIO do bloco que 
else:
	validaIp(remoteServer.split("."))
	remoteServerIP  = socket.gethostbyname(remoteServer)

# PERGUNTA qual a porta a ser varrida
if(args.prange is not None):
	remotePort      = args.prange
else:
	remotePort      = input("Digite a porta a ser scaneada: ")

# INICIO do bloco que identifica quantas portas foram digitadas
if remotePort.find(",") > 0:
	portas = remotePort.split(",")
	allPorts = list()
	for ports in portas:
		if ports.find("-") > 0:
			iniPorts,fimPorts = ports.split("-")
			allPorts += [p for p in range(int(iniPorts),int(fimPorts))]
			allPorts.append(fimPorts)
		else:
			allPorts.append(ports)
elif remotePort.find("-") > 0:
	iniPorts,fimPorts = remotePort.split("-")
	allPorts = [p for p in range(int(iniPorts),int(fimPorts))]
	allPorts.append(fimPorts)
else:
	portaRemota = remotePort


# Check what time the scan started
t1 = datetime.now()

# We also put in some error handling for catching errors
try:
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	# INICIO do bloco que ve quantos hosts fora digitados
	if allHosts==None:
		# Apenas um Host
		cabecalho(remoteServerIP)
		# Percorre as portas digitadas
		if allPorts==None:
			result = sock.connect_ex((remoteServerIP, int(portaRemota)))
			if result == 0:
				print ("Port {}: 	 Open".format(portaRemota))
				if(args.lfile is not None):
					arq.write ("Port {}: 	 Open".format(portaRemota))
					arq.write ("\n")
			else:
				print ("Port {}: 	 Close".format(portaRemota))
				if(args.lfile is not None):
					arq.write ("Port {}: 	 Close".format(portaRemota))
					arq.write ("\n")
		# Mais de uma porta digitada
		else:
			for portaRemota in allPorts:
				result = sock.connect_ex((remoteServerIP, int(portaRemota)))
				if result == 0:
					print ("Port {}: 	 Open".format(portaRemota))
					if(args.lfile is not None):
						arq.write ("Port {}: 	 Open".format(portaRemota))
						arq.write ("\n")
				else:
					print ("Port {}: 	 Close".format(portaRemota))
					if(args.lfile is not None):
						arq.write ("Port {}: 	 Close".format(portaRemota))
						arq.write ("\n")
	else:
		# Mais de um host
		# Percorre todos os Hosts
		for ipHost in allHosts:
			remoteServerIP  = socket.gethostbyname(str(ipHost))
			cabecalho(remoteServerIP)
			# Percorre as portas digitadas
			if allPorts==None:
				result = sock.connect_ex((remoteServerIP, int(portaRemota)))
				if result == 0:
					print ("Port {}: 	 Open".format(portaRemota))
					if(args.lfile is not None):
						arq.write ("Port {}: 	 Open".format(portaRemota))
						arq.write ("\n")
				else:
					print ("Port {}: 	 Close".format(portaRemota))
					if(args.lfile is not None):
						arq.write ("Port {}: 	 Close".format(portaRemota))
						arq.write ("\n")
			# Mais de uma porta digitada
			else:
				for portaRemota in allPorts:
					result = sock.connect_ex((remoteServerIP, int(portaRemota)))
					if result == 0:
						print ("Port {}: 	 Open".format(portaRemota))
						if(args.lfile is not None):
							arq.write ("Port {}: 	 Open".format(portaRemota))
							arq.write ("\n")
					else:
						print ("Port {}: 	 Close".format(portaRemota))
						if(args.lfile is not None):
							arq.write ("Port {}: 	 Close".format(portaRemota))
							arq.write ("\n")
			
	
	sock.close()

except KeyboardInterrupt:
	print ("Cancelado pelo comando Ctrl+C")
	if(args.lfile is not None):
		arq.write ("Cancelado pelo comando Ctrl+C")
		arq.write ("\n")
	sys.exit()

except socket.gaierror:
	print ('Hostname Nao pode ser resolvido. Saindo')
	if(args.lfile is not None):
		arq.write ('Hostname Nao pode ser resolvido. Saindo')
		arq.write ("\n")
	sys.exit()

except socket.error:
	print ("Nao foi possivel conectar ao servidor")
	if(args.lfile is not None):
		arq.write ("Nao foi possivel conectar ao servidor")
		arq.write ("\n")
	sys.exit()

# Checking the time again
t2 = datetime.now()

# Calculates the difference of time, to see how long it took to run the script
total =  t2 - t1

# Printing the information to screen
print ('Scanning completo em: ', total)
if(args.lfile is not None):
	arq.write ('Scanning completo em: ' + str(total))
	arq.write ("\n")
	arq.close()